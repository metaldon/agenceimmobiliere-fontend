import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'agenceImmobiliereApp';



  images = [
    {
      imageSrc:
        '../assets/index/img/h4-slide.png',
      imageAlt: 'nature1',
    },
    {
      imageSrc:
        '../assets/index/img/h4-slide2.png',
      imageAlt: 'nature2',
    },
    {
      imageSrc:
        '../assets/index/img/h4-slide3.png',
      imageAlt: 'nature3',
    },
    {
      imageSrc:
        '../assets/index/img/h4-slide4.png',
      imageAlt: 'nature4',
    },
  ]
 
}
