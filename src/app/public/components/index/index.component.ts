import { Component, Input, OnInit } from '@angular/core';

interface carouselImage {
  imageSrc: String;
  imageAlt: String;
}


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor() { }

  @Input() images: carouselImage[] = [];
  @Input() indicators = true;
  @Input() controls = true;
  @Input() autoSlide = true;
  @Input() slideInterval = 3000; //default to 3 second

  selectedIndex = 0;

  ngOnInit(): void {
    if(this.autoSlide){
      this.autoSlideImages();
    }
  }
// changes slides in every 3 second
  autoSlideImages(): void {
    setInterval(() => {
      this.onNextClick();
    }, this.slideInterval)
  }
  /* SETS INDEX OF IMAGE ON DOT/ INDICATOR CLICK*/
  selectedImage(index: number): void {
    this.selectedIndex = index;
  }
    onPrevClick(): void {
      if(this.selectedIndex === 0) {
        this.selectedIndex = this.images.length -1;
    } else {
      this.selectedIndex--;
    }
  }
  onNextClick(): void {
    if(this.selectedIndex === this.images.length -1) {
      this.selectedIndex =0;
  } else {
    this.selectedIndex++;
  }
}
}
