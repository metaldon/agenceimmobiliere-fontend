import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AdminComponent } from './private/components/admin/admin.component';
import { IndexComponent } from './public/components/index/index.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    IndexComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
