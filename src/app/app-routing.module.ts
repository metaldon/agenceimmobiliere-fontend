import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [

/*  { path: 'biens', loadChildren: () => import('./modules/biens/biens.module').then(mod => mod.BiensModule) } */

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
